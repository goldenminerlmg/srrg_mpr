include_directories(".")

add_executable(mpr_tracker_app mpr_tracker_app.cpp)
target_link_libraries(mpr_tracker_app
  mpr
  ${OpenCV_LIBS}
  ${catkin_LIBRARIES}
 )


add_executable(kitti_velodyne_tracker kitti_velodyne_tracker.cpp)
target_link_libraries(kitti_velodyne_tracker
  mpr
  ${OpenCV_LIBS}
  ${catkin_LIBRARIES}
 )
